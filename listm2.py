a=['A','B','C']
b=[1,2,3,True,False]

# ab='Python'
# print('z' in ab)
#print(2 in b)
# print(2 not in b)

# concatenate lists
c=a+b

# multiply lists
m=a*4

# clear list
# b.clear()
# print(b)

# append vs extend
# app=a.append(b)
# print(a)
# app=a.extend(b)
# print(a)

# sort
ele=[1,432,54,23,5,8,6,2]
#ele.sort()
ele.reverse()
c=ele.copy()
print(c)

