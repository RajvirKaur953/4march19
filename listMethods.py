#CREATING A LIST
colors=[]

#ADD ELEMENTS IN LIST
colors.append('red')
colors.append('green')
colors.append('white')
# colors.append([0,1])

# change elements in list
# colors[1]='gray'

# REMOVE ELEMENTS FROM  LAST LOCATION
#color.pop()
# ADD ELEMENT AT SPECIFIC LOCATION
# colors.insert(2,'white')
# remove particular element
# colors.remove('white')
# del colors
print('Before',colors)
del colors[1]
print('After',colors)

print(len(colors))


